package by.javatr.day3.task;

public class Basket {
    private Ball[] balls;

    public Basket(int size) {
        balls = new Ball[size];
    }

    public boolean addBall(Ball ball) {
        for (int i = 0; i < balls.length; i++) {
            if (balls[i] == null) {
                balls[i] = ball;
                return true;
            }
        }
        System.out.println("Basket is full");
        return false;
    }

    public int calcWeight() {
        int result = 0;
        for (Ball ball : balls) {
            if (ball != null) {
                result += ball.getWeight();
            }
        }
        return result;
    }

    public int getCountOfBlue() {
        int result = 0;
        for (Ball ball : balls) {
            if (ball != null && ball.getColor() == Color.BLUE) {
                result++;
            }
        }
        return result;
    }
}
