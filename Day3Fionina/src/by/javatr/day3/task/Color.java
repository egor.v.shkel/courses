package by.javatr.day3.task;

public enum Color {
    RED, BLUE, GREEN, ORANGE, YELLOW, PINK, WHITE, BLACK;
}
