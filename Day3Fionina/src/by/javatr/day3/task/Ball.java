package by.javatr.day3.task;

public class Ball {
    private Color color;
    private int weight;

    public Ball(Color color, int weight) {
        this.color = color;
        this.weight = weight;
        if (color == null) {
            throw new IllegalArgumentException("Error, ball should have colour");
        }
        if (weight <= 0) {
            throw new IllegalArgumentException("Error, weight should be positive");
        }
    }

    public Color getColor() {
        return color;
    }

    public int getWeight() {
        return weight;
    }
}


