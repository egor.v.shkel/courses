package by.javatr.day3.task;

public class Main {
    public static void main(String[] args) {
        Basket basket = new Basket(9);
        basket.addBall(new Ball(Color.RED, 3));
        basket.addBall(new Ball(Color.BLUE, 4));
        basket.addBall(new Ball(Color.GREEN, 5));
        basket.addBall(new Ball(Color.ORANGE, 2));
        basket.addBall(new Ball(Color.BLUE, 5));
        basket.addBall(new Ball(Color.BLACK, 1));
        basket.addBall(new Ball(Color.WHITE, 2));
        basket.addBall(new Ball(Color.PINK, 8));
        basket.addBall(new Ball(Color.YELLOW, 1));
        basket.addBall(new Ball(Color.BLUE, 3));

        int weight = basket.calcWeight();
        int countOfBlue = basket.getCountOfBlue();

        System.out.println("Ball's weight: " + weight);
        System.out.println("Count of blue balls: " + countOfBlue);
    }

}

