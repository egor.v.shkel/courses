package by.javatr.task3.util;

public class AreaCalculator {
    public double getInternalSquareArea(double externalSquareArea) {
        return externalSquareArea / 2;
    }

    public double getAreaDifference(double externalSquareArea, double internalSquareArea) {
        return externalSquareArea / internalSquareArea;
    }
}
