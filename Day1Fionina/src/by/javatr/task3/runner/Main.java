package by.javatr.task3.runner;

import by.javatr.task3.util.AreaCalculator;

public class Main {
    public static void main(String[] args) {
        AreaCalculator areaCalculator = new AreaCalculator();
        double externalSquareArea = 25.5;
        double internalSquareArea = areaCalculator.getInternalSquareArea(externalSquareArea);
        double areaDifference = areaCalculator.getAreaDifference(externalSquareArea, internalSquareArea);
    }
}
