package by.javatr.task1.runner;

import by.javatr.task1.util.GuessNumberHelper;

public class Main {
    public static void main(String[] args) {
        GuessNumberHelper guessNumberHelper = new GuessNumberHelper();
        int userNumber = 123;
        int result = guessNumberHelper.getLastNumeralOfSquaredNumber(userNumber);
    }
}