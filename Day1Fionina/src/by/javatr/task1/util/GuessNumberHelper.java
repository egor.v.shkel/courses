package by.javatr.task1.util;

public class GuessNumberHelper {
    public int getLastNumeralOfSquaredNumber(int lastNumeralOfUserNumber) {
        int squaredLastNum = getSquared(lastNumeralOfUserNumber);
        return getLastNumeral(squaredLastNum);
    }

    private int getSquared(int number) {
        return number * number;
    }

    private int getLastNumeral(int number) {
        return number % 10;
    }
}
