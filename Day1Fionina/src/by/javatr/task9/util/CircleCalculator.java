package by.javatr.task9.util;

public class CircleCalculator {
    public double getCircumference(double radius) {
        return 2 * radius * Math.PI;
    }

    public double getCircleArea(double radius) {
        return Math.PI * radius * radius;
    }
}
