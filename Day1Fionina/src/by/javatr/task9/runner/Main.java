package by.javatr.task9.runner;

import by.javatr.task9.util.CircleCalculator;

public class Main {
    public static void main(String[] args) {
        CircleCalculator circleCalculator = new CircleCalculator();
        double radius = 33.3;
        double circumference = circleCalculator.getCircumference(radius);
        double circleArea = circleCalculator.getCircleArea(radius);

    }
}
