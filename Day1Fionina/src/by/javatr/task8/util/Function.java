package by.javatr.task8.util;

public class Function {
    public double getResult(double x) {
        if (x >= 3) {
            return (-(x * x) + 3 * x + 9);
        } else {
            return 1 / (x * x * x - 6);
        }
    }
}
