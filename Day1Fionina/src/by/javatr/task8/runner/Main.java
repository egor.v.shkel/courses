package by.javatr.task8.runner;

import by.javatr.task8.util.Function;

public class Main {
    public static void main(String[] args) {
        Function function = new Function();
        double x = 1;
        double functionResult = function.getResult(x);
        System.out.println(functionResult);
    }
}
