package by.javatr.task2.runner;

import by.javatr.task2.util.DayCalculator;

public class Main {
    public static void main(String[] args) {
        DayCalculator dayCalculator = new DayCalculator();

        int year = 1991;
        boolean leapYear = dayCalculator.isItLeapYear(year);
        int month = 5;
        int numberOfDays = dayCalculator.getNumberOfDays(year, month);

    }
}