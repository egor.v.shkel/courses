package by.javatr.task2.util;

public class DayCalculator {
    public boolean isItLeapYear(int year) {
        if (year % 4 == 0) {
            if ((year % 100 != 0) || (year % 400 == 0)) {
                return true;
            }
        }
        return false;
    }

    public int getNumberOfDays(int year, int month) {
        if (month > 12 || month < 1) {
            return 0;
        }

        if (month == 2) {
            return getNumberOfDaysInFebruary(year);
        } else if (month <= 7) {
            return getNumberOfDaysInMonth(month, false);
        } else {
            return getNumberOfDaysInMonth(month, true);
        }

    }

    private int getNumberOfDaysInFebruary(int year) {
        if (isItLeapYear(year)) {
            return 29;
        } else {
            return 28;
        }
    }

    private int getNumberOfDaysInMonth(int month, boolean afterJuly) {
        if (afterJuly) {
            return getNumberOfDays(month, 31, 30);
        } else {
            return getNumberOfDays(month, 30, 31);
        }
    }

    private int getNumberOfDays(int month, int daysInEvenMonth, int daysInUnEvenMonth) {
        if (month % 2 == 0) {
            return daysInEvenMonth;
        } else {
            return daysInUnEvenMonth;

        }
    }
}
