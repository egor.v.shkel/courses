package by.javatr.task10.runner;


import by.javatr.task10.util.Settings;

public class Main {
    public static void main(String[] args) {
        Settings settings = new Settings(1, 20, 2);
        Settings.TangentCalculator tangentCalculator = new Settings.TangentCalculator(settings);
        double[][] result = tangentCalculator.getFunctionResult();
        tangentCalculator.printResult(result);
        settings = new Settings(44.1, 51.2, 2.4);
        tangentCalculator.setSettings(settings);
        result = tangentCalculator.getFunctionResult();
        tangentCalculator.printResult(result);
    }
}


