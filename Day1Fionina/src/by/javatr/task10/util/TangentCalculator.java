public static class TangentCalculator {
    private Settings settings;

    public TangentCalculator(Settings settings) {
        this.settings = settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public double[][] getFunctionResult() {
        if (settings == null) {
            return new double[0][0];
        }
        int capacity = (int) ((settings.getEnd() - settings.getStart()) / settings.getStep()) + 1;
        double[][] result = new double[capacity][2];
        int currentRow = 0;
        for (double i = settings.getStart(); i <= settings.getEnd(); i = i + settings.getStep()) {
            result[currentRow][0] = i;
            result[currentRow][1] = Math.tan(i);
            currentRow++;
        }
        return result;
    }

    public void printResult(double[][] result) {
        String format = "|%1$-30s|%2$-30s|\n";
        System.out.format(format, "Значение", "Тангенс");
        System.out.format(format, "______________________________", "______________________________");
        for (double[] row : result) {
            System.out.format(format, row[0], row[1]);
            System.out.format(format, "______________________________", "______________________________");
        }
    }
}