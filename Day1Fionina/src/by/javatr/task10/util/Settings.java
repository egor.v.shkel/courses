package by.javatr.task10.util;

public class Settings {
    private double start;

    private double end;

    private double step;

    public Settings(double start, double end, double step) {
        this.start = start;
        this.end = end;
        this.step = step;
    }

    public double getStart() {
        return start;
    }

    public double getEnd() {
        return end;
    }

    public double getStep() {
        return step;
    }

}
