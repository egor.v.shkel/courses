package by.javatr.task4.runner;

import by.javatr.task4.util.NumberAnalyzer;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        NumberAnalyzer numberAnalyzer = new NumberAnalyzer();
        int countOfNumbers = 4;
        int[] numbers = new int[countOfNumbers];
        numbers[0] = 5;
        numbers[1] = 6;
        numbers[2] = 8;
        numbers[3] = 9;
        int countOfEven = 2;
        boolean result = numberAnalyzer.doesContainEven(numbers, countOfEven);

    }
}
