package by.javatr.task4.util;

public class NumberAnalyzer {
    public boolean doesContainEven(int[] numbers, int countOfEven) {
        int evenNumbers = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                evenNumbers++;
            }
            if (evenNumbers >= countOfEven) {
                return true;
            }

        }
        return false;
    }
}
