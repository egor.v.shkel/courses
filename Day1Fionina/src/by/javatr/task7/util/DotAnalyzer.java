package by.javatr.task7.util;

public class DotAnalyzer {
    public Dot closerToOrigin(Dot first, Dot second) {
        int firstSquareDistance = first.getX() * first.getX() + first.getY() * first.getY();

        int secondSquareDistance = second.getX() * second.getX() + second.getY() * second.getY();

        if (firstSquareDistance == secondSquareDistance) {
            return null;
        } else if (firstSquareDistance < secondSquareDistance) {
            return first;
        } else {
            return second;
        }
    }

    public static class Dot {
        private String name;

        private int x;

        private int y;

        public Dot(String name, int x, int y) {
            this.name = name;
            this.x = x;
            this.y = y;
        }

        public String getName() {
            return name;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}

