package by.javatr.task7.runner;

import by.javatr.task7.util.DotAnalyzer;

public class Main {
    public static void main(String[] args) {
        DotAnalyzer dotAnalyzer = new DotAnalyzer();
        DotAnalyzer.Dot dotA = new DotAnalyzer.Dot("A", -12, 5);
        DotAnalyzer.Dot dotB = new DotAnalyzer.Dot("B", 9, -1);
        DotAnalyzer.Dot result = dotAnalyzer.closerToOrigin(dotA, dotB);

    }
}

