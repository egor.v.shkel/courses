package by.javatr.task5.runner;

import by.javatr.task5.util.PerfectNumberAnalyzer;

public class Main {
    public static void main(String[] args) {
        PerfectNumberAnalyzer perfectNumberAnalyzer = new PerfectNumberAnalyzer();
        int perfectNumber = 6;
        int regularNumber = 33;

        boolean result = perfectNumberAnalyzer.isPerfect(perfectNumber);
        System.out.println("Число " + perfectNumber + " является совершенным? " + result);

        result = perfectNumberAnalyzer.isPerfect(regularNumber);

        System.out.println("Число " + regularNumber + " является совершенным? " + result);
    }
}