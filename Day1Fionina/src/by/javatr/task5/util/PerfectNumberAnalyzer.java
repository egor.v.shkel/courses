package by.javatr.task5.util;

public class PerfectNumberAnalyzer {
    public boolean isPerfect(int number) {
        if (number < 1) {
            return false;
        }
        int dividersSum = getDividersSum(number);

        return number == dividersSum;
    }

    private int getDividersSum(int number) {
        int result = 0;

        for (int i = 1; i < number; i++) {
            if (number % i == 0) {
                result += i;
            }
        }

        return result;
    }
}