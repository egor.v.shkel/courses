package by.javatr.task6.util;

public class TimeCalculator {
    public int getCountOfCompletedSeconds(int secondNumber) {
        return secondNumber - 1;
    }

    public int getCountOfCompletedMinutes(int secondNumber) {
        int countOfCompletedSeconds = getCountOfCompletedSeconds(secondNumber);
        return countOfCompletedSeconds / 60;
    }

    public int getCountOfCompletedHours(int secondNumber) {
        int countOfCompletedMinutes = getCountOfCompletedMinutes(secondNumber);
        return countOfCompletedMinutes / 60;
    }
}
