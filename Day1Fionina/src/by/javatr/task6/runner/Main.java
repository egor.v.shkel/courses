package by.javatr.task6.runner;

import by.javatr.task6.util.TimeCalculator;

public class Main {
    public static void main(String[] args) {
        TimeCalculator timeCalculator = new TimeCalculator();

        int secondNumber = 1111111;
        int countOfCompletedSeconds = timeCalculator.getCountOfCompletedSeconds(secondNumber);
        int countOfCompletedMinutes = timeCalculator.getCountOfCompletedMinutes(secondNumber);
        int countOfCompletedHours = timeCalculator.getCountOfCompletedHours(secondNumber);
    }
}
